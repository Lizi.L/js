function generateRandomNumbers() {
    const randomNumbers = [];
    for (let i = 0; i < 30; i++) {
        const randomNumber = Math.floor(Math.random() * 31);
        randomNumbers.push(randomNumber);
    }
    return randomNumbers;
}

function simulateButtonClick() {
    const divElement = document.getElementById('buttondiv');
    const buttonElement = divElement.querySelector('#myButton');

    const randomNumbers = generateRandomNumbers();
    console.log('Random Numbers:', randomNumbers);
    };

document.addEventListener('DOMContentLoaded', simulateButtonClick);