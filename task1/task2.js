function printNaturalNumbers() {
    const n = prompt("Enter the value of n:");
  
    const numN = parseInt(n);
  
    if (isNaN(numN) || numN <= 0) {
      console.log("Please enter a valid positive number.");
      return;
    }
  
    for (let i = 1; i <= numN; i++) {
      console.log(i);
    }
  }
  
  printNaturalNumbers();
  