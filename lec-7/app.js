function generate_tb(){
    let row_n = document.getElementById('row_n')
    let col_n = document.querySelector('#col_n')
    let sym_n = document.getElementsByTagName('input') [2]
    let tb = "<table>"
        for (let i = 0; i <row_n.value; i++){
            tb += "<tr>"
            for (let j = 0; j < col_n.value;j++){
                tb += "<td>"
                tb += generate_r_diff_string(sym_n.value)
                tb += "</td>"
            }
            tb += "</tr>"
        }
    tb += "</table>"
    let div_tb = document.getElementById('div-tb')
    div_tb.innerHTML = tb 

}

function generate_r_diff_string(N){
    let abc = "abcdefghijklmnopqrstuvwxyz"
    let w = " "
    while(w.length!=N){
        let s = abc[Math.floor(Math.random() *abc.length)]
        if (w.indexOf(s) == -1 ){
            w += s 
        }
    }

    return w 
}

generate_r_diff_string(7)