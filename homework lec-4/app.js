    // task 1 
    document.addEventListener("DOMContentLoaded", function () {
        const daysOfWeekInput = document.getElementById("daysOfWeek");
        const buttons = document.querySelectorAll("#Button");
      
        const days = ["ორშაბათი", "სამშაბათი", "ოთხშაბათი", "ხუთშაბათი", "პარასკევი", "შაბათი", "კვირა"];
      
        buttons.forEach((button, index) => {
          button.addEventListener("click", function () {
            daysOfWeekInput.value = days[index];
          });
        });
      });
    
    // task 5
    
    // function generateRandomNumbers() {
    //   const randomNumbers = [];
    //   for (let i = 0; i < 30; i++) {
    //     const randomNumber = Math.floor(Math.random() * 31); 
    //     randomNumbers.push(randomNumber);
    //   }
    //   return randomNumbers;
    // }

    // function simulateButtonClick() {
    //   const divElement = document.getElementById('buttondiv');
    //   const buttonElement = divElement.querySelector('#myButton');
      
    //   const randomNumbers = generateRandomNumbers();
    //   console.log('Random Numbers:', randomNumbers);

    //   buttonElement.click();
    // }

    