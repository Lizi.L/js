function getUnicodeCodes(inputString) {
    const unicodeCodes = [];
  
    for (let i = 0; i < inputString.length; i++) {
      const code = inputString.charCodeAt(i);
      unicodeCodes.push(code);
    }
  
    return unicodeCodes;
  }
  
  const inputString = "Hello, World!";
  const codes = getUnicodeCodes(inputString);
  console.log(codes);
  