const startDate = new Date('2016-01-01T00:00:00Z');

const currentDate = new Date();

const timeDifference = currentDate - startDate;

const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));
const hours = Math.floor((timeDifference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
const minutes = Math.floor((timeDifference % (1000 * 60 * 60)) / (1000 * 60));
const seconds = Math.floor((timeDifference % (1000 * 60)) / 1000);

console.log(`${days} days, ${hours} hours, ${minutes} minutes, ${seconds} seconds have passed since January 1, 2016.`);
