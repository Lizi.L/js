const field = document.getElementById("field");
const ctx = field.getContext("2d");

const square = {
  x: 0,
  y: 0,
  width: 50,
  height: 50,
  speed: 5,
  color: "green"
};

function drawSquare() {
  ctx.fillStyle = square.color;
  ctx.fillRect(square.x, square.y, square.width, square.height);
}

function moveSquare(dx, dy) {
  square.x += dx * square.speed;
  square.y += dy * square.speed;
}

function handleKeyDown(event) {
  switch (event.key) {
    case "ArrowUp":
      moveSquare(0, -1);
      break;
    case "ArrowDown":
      moveSquare(0, 1);
      break;
    case "ArrowLeft":
      moveSquare(-1, 0);
      break;
    case "ArrowRight":
      moveSquare(1, 0);
      break;
  }
}

function updateGame() {
  ctx.clearRect(0, 0, field.width, field.height);
  drawSquare();
}

document.addEventListener("keydown", handleKeyDown);
setInterval(updateGame, 1000 / 60);
