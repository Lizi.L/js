const engToGeoMap = {
    'a': 'ა', 'b': 'ბ', 'c': 'ც', 'd': 'დ', 'e': 'ე',
    'f': 'ფ', 'g': 'გ', 'h': 'ჰ', 'i': 'ი', 'j': 'ჯ',
    'k': 'კ', 'l': 'ლ', 'm': 'მ', 'n': 'ნ', 'o': 'ო',
    'p': 'პ', 'q': 'ქ', 'r': 'რ', 's': 'ს', 't': 'ტ',
    'u': 'უ', 'v': 'ვ', 'w': 'წ', 'x': 'ხ', 'y': 'ყ',
    'z': 'ზ'
};

const geoToEngMap = {};
for (const key in engToGeoMap) {
    geoToEngMap[engToGeoMap[key]] = key;
}

function convertToGeorgian(text) {
    return text.split('').map(char => engToGeoMap[char] || char).join('');
}

function convertToEnglish(text) {
    return text.split('').map(char => geoToEngMap[char] || char).join('');
}

function convertText() {
    const inputText = document.getElementById('inputText').value;
    const outputTextElementGeorgian = document.getElementById('outputTextGeorgian');
    const outputTextElementEnglish = document.getElementById('outputTextEnglish');
    
    if (outputTextElementGeorgian && outputTextElementEnglish) {
        const outputTextGeorgian = convertToGeorgian(inputText);
        const outputTextEnglish = convertToEnglish(inputText);
        
        outputTextElementGeorgian.innerText = outputTextGeorgian;
        outputTextElementEnglish.innerText = outputTextEnglish;
    }
}