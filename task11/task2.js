function checkTitle(title) {
    if (title.includes('#') || title.includes('$') || title.includes('!')) {
        return '';
    }
    return title;
}

function blurPages() {
    return Math.floor(Math.random() * 401) + 100; 
}

function checkPrice(price) {
    return /^\d{2}$/.test(price);
}

function calculateCirculation(pages) {
    return Math.floor(pages * 0.2);
}

function getRandomAuthor() {
    const authors = ["Nika Tavadze", "Nino Gudadze", "Vako Narimanidze", "Lika Nioradze"];
    const randomIndex = Math.floor(Math.random() * authors.length);
    return authors[randomIndex];
}

function submitForm() {
    const titleInput = document.getElementById('title').value;
    const pagesInput = document.getElementById('pages').value;
    const authorInput = document.getElementById('author').value;
    const priceInput = document.getElementById('price').value;

    const processedTitle = checkTitle(titleInput);
    const blurredPages = blurPages();
    const randomAuthor = getRandomAuthor();
    const validPrice = checkPrice(priceInput);
    const circulation = calculateCirculation(blurredPages);

    document.getElementById('output').innerHTML = `
        <strong>Processed Title:</strong> ${processedTitle}<br>
        <strong>Blurred Pages:</strong> ${blurredPages}<br>
        <strong>Random Author:</strong> ${randomAuthor}<br>
        <strong>Valid Price:</strong> ${validPrice ? 'Yes' : 'No'}<br>
        <strong>Circulation:</strong> ${circulation}
    `;
}