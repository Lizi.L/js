function generateRandomNumbers() {
    const randomNumbers = [];
  
    for (let i = 0; i < 40; i++) {
      const randomNumber = Math.random(); 
      randomNumbers.push(randomNumber);
    }
  
    return randomNumbers;
  }

  const randomNumbersArray = generateRandomNumbers();
  console.log(randomNumbersArray);
  