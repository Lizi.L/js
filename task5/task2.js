var userConfirmed = window.confirm("გამარჯობაა, გსურთ გაიგოთ ვებ გვერდზე უფრო მეტი?");

if (userConfirmed) {
    document.body.innerHTML += "<h1 style='color: white;'><span style='color: red;'> გილოცავთ,</span> თქვენ სწორი არჩევანი გააკეთეთ</h1>";
    document.body.innerHTML += "<h4 style='color: yellow;'>ამ ვებ გვერდზე მრავალი საინტერესო მაგალითი შეგხვდებათ ვებ გვერდის აწყობის შესახებ</h4>";
    document.body.innerHTML += "<h4 style='color: white;'>ვებ გვერდის აწყობის ტექნოლოგიები: </h4>";
    document.body.innerHTML += "<li style='color: white;'>HTML - საფუძვლები</li>";
    document.body.innerHTML += "<li style='color: white;'>CSS - სტილები</li>";
    document.body.innerHTML += "<li style='color: white;'>JavaScript - გაფორმება </li>";
    document.body.innerHTML += "<li style='color: white;'>PHP - პროგრამირება</li>";


} else {
    document.body.innerHTML += "<h1 style='color: green;'>დაინტერესდით ვებ გვერდის აწყობის ტექნოლოგიით.</h1>";
    document.body.innerHTML += "<h3 style='color: white;'>დაეუფლეთ მსოფიოში ერთ-ერთ ყველაზე მოთხოვნად პროფესიას</h3>";
    document.body.innerHTML += "<h3 style='color: yellow;'>ისწავლე და დასაქმდი! </h3>";
}
