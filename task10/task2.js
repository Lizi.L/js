const currentDate = new Date();

const userDateInput = prompt('Enter a date (YYYY-MM-DD):');
const userDate = new Date(userDateInput);

if (isNaN(userDate.getTime())) {
  console.log('Invalid date entered.');
} else {
  const beginningOfLastYear = new Date(currentDate.getFullYear() - 1, 0, 1);

  const timeDifference = userDate - beginningOfLastYear;

  const days = Math.floor(timeDifference / (1000 * 60 * 60 * 24));

  console.log(`Number of days from the beginning of the last year to ${userDate.toDateString()}: ${days} days.`);
}
