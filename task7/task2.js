document.addEventListener("DOMContentLoaded", function() {
    const canvas = document.getElementById("colorCanvas");
    const ctx = canvas.getContext("2d");

    const squareSize = 20; 
    const rows = canvas.height / squareSize;
    const cols = canvas.width / squareSize;

    function getRandomColor() {
      const colors = ["green", "blue", "red", "yellow"];
      return colors[Math.floor(Math.random() * colors.length)];
    }

    function drawRandomSquares() {
      for (let row = 0; row < rows; row++) {
        for (let col = 0; col < cols; col++) {
          const color = getRandomColor();
          ctx.fillStyle = color;
          ctx.fillRect(col * squareSize, row * squareSize, squareSize, squareSize);
        }
      }
    }

    drawRandomSquares();
  });