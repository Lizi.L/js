function isEmpty(value) {
    return value.trim() === '';
  }

  function isIdValid(id) {
    return /^\d{11}$/.test(id);
  }

  function getCurrentDate() {
    const today = new Date();
    const day = today.getDate();
    const month = today.getMonth() + 1;
    const year = today.getFullYear();
    return `${day}:${month}:${year}`;
  }

  function hasAtSymbol(email) {
    return email.includes('@');
  }

  function validateForm() {
    const name = document.getElementById('name').value;
    const lastName = document.getElementById('lastName').value;
    const idNumber = document.getElementById('idNumber').value;
    const registrationDate = document.getElementById('registrationDate');
    const email = document.getElementById('email').value;

    let isValid = true;

    if (isEmpty(name)) {
      alert('Name cannot be empty');
      isValid = false;
    }

    if (isEmpty(lastName)) {
      alert('Last Name cannot be empty');
      isValid = false;
    }

    if (!isIdValid(idNumber)) {
      alert('ID Number must be 11 digits');
      isValid = false;
    }

    registrationDate.value = getCurrentDate();

    if (!hasAtSymbol(email)) {
      alert('E-mail must contain the "@" symbol');
      isValid = false;
    }

    if (isValid) {
      alert('Form submitted successfully!');
    }
  }