function printColorSquares(colors) {
    let index = 0;
  
    function changeColor() {
      document.querySelectorAll('.colorSquare').forEach(square => {
        square.style.backgroundColor = colors[index];
      });
      index = (index + 1) % colors.length;
    }
  
    setInterval(changeColor, 1000);
  
    function createColorSquares() {
      const colorSquaresContainer = document.getElementById('colorSquares');
      colorSquaresContainer.innerHTML = '';
      
      colors.forEach(color => {
        const square = document.createElement('div');
        square.className = 'colorSquare';
        square.style.backgroundColor = color;
        colorSquaresContainer.appendChild(square);
      });
    }
  
    createColorSquares();
  }
  
  printColorSquares(['#ff0000', '#00ff00', '#0000ff']);