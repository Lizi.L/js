function createTable() {
    let row_n = document.getElementById('row_n').value;
    let col_n = document.getElementById('col_n').value;
    let tb = "<table>";

    for (let i = 0; i < row_n; i++) {
        tb += "<tr>";

        for (let j = 0; j < col_n; j++) {
            tb += "<td></td>";
        }

        tb += "</tr>";
    }

    tb += "</table>";
    let div_tb = document.getElementById('div-tb');
    div_tb.innerHTML = tb;
}
